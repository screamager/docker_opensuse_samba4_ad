# docker_opensuse_samba4_ad

It's the base opensuse container with samba 4 as domain controller.

Edit to section global for loging.
```
log level = 3 passdb:5 auth:5
```

For routing on host. The interfaces may be differ.
```
$ iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
$ iptables -t nat -A POSTROUTING -o docker0 -j MASQUERADE
```
For routing on gate.
```
$ route add -host <container ip> gw <host ip>
```

How to using:
```
./build.sh          # build project
./run.sh            # run contaiter
./ssh_connect.sh    # enter to container thro ssh
```