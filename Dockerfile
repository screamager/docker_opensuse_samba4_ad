FROM opensuse:tumbleweed

ENV     container docker

# install systemd
RUN     zypper -n install systemd; \
            (cd /usr/lib/systemd/system/sysinit.target.wants/; for i in *; \
                do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
            rm -f /usr/lib/systemd/system/multi-user.target.wants/*;\
            rm -f /etc/systemd/system/*.wants/*;\
            rm -f /usr/lib/systemd/system/local-fs.target.wants/*; \
            rm -f /usr/lib/systemd/system/sockets.target.wants/*udev*; \
            rm -f /usr/lib/systemd/system/sockets.target.wants/*initctl*; \
            rm -f /usr/lib/systemd/system/basic.target.wants/*;\
            rm -f /usr/lib/systemd/system/anaconda.target.wants/*

# start systemd
VOLUME  [ "/sys/fs/cgroup" ]
CMD     ["/usr/lib/systemd/systemd", "--system"]

# update OS
RUN     zypper -n dup; \
        zypper -n up; \
        zypper clean

# install programs
RUN     zypper -n in nano openssh sshfs aaa_base glibc-locale glibc-i18ndata \
        iproute2 sysstat tcpdump iptables tar psmisc less ed \
        net-tools bind-utils net-tools-deprecated system-user-nobody; \
        zypper clean

# set locale ru_RU.UTF-8
RUN     echo -e "/^RC_LANG/s/.*/RC_LANG=\"ru_RU\.UTF-8\"\n.\nw" | ed -s /etc/sysconfig/language
RUN     localedef -c -i ru_RU -f UTF-8 ru_RU.UTF-8

# install samba
ARG     SMBPACKVER
RUN     zypper ar -f http://download.opensuse.org/repositories/home:/jniltinho/openSUSE_Tumbleweed/ samba
RUN     zypper --gpg-auto-import-keys -n in $SMBPACKVER

# prepare configure samba
RUN     echo 'export PATH=/opt/samba4/bin/:/opt/samba4/sbin/:$PATH' > /etc/profile.d/samba.sh
RUN     chmod +x /etc/profile.d/samba.sh
ENV     PATH="/opt/samba4/bin/:/opt/samba4/sbin/:${PATH}"
RUN     mv /etc/krb5.conf /etc/krb5.conf.orig

# set password for the root
ARG     ROOTPASS
RUN     echo "root:$ROOTPASS" | chpasswd

# create samba domain
ARG     HOSTNAME
ARG     ADMINPASS
ARG     DOMAIN
ARG     NETBIOSNAME
RUN     samba-tool domain provision --server-role=dc --use-rfc2307 --dns-backend=SAMBA_INTERNAL \
            --realm=$DOMAIN --domain=$NETBIOSNAME --host-name=$HOSTNAME --adminpass=$ADMINPASS
RUN     ln -sf /opt/samba4/private/krb5.conf /etc/krb5.conf
RUN     systemctl enable samba4.service

# edit smb.conf
COPY    smb_add_param_to_section.py /opt/samba4/bin/
RUN     smb_add_param_to_section.py /opt/samba4/etc/smb.conf global '# need for Win XP'
RUN     smb_add_param_to_section.py /opt/samba4/etc/smb.conf global 'guest ok = No'
RUN     smb_add_param_to_section.py /opt/samba4/etc/smb.conf global '#log level = 3 passdb:5 auth:5'

## add qmail
COPY    qmail.schema /tmp/
COPY    qmail.samba4-update-user-oc.ldif /tmp/
RUN     sed -i.orig -e "s/DC=.*/$(echo $DOMAIN |sed 's/^/DC=/;s/\./,DC=/')/" \
            /tmp/qmail.samba4-update-user-oc.ldif
COPY    qmail.samba4-qmailUser-downgrade.ldif /tmp/
RUN     sed -i.orig -e "s/DC=.*/$(echo $DOMAIN |sed 's/^/DC=/;s/\./,DC=/')/" \
            /tmp/qmail.samba4-qmailUser-downgrade.ldif
# convert schema to ldif
RUN     oLschema2ldif -b $(echo $DOMAIN |sed 's/^/DC=/;s/\./,DC=/') \
            -I /tmp/qmail.schema -O /tmp/qmail.samba4.ldif
# brick schema on atrributes and object classes
RUN     perl -00 -wne 'print if /attributeSchema/' /tmp/qmail.samba4.ldif > /tmp/qmail.samba4-at.ldif
RUN     perl -00 -wne 'print if /classSchema/' /tmp/qmail.samba4.ldif > /tmp/qmail.samba4-oc.ldif
# add schema to samba4.
# NOTE '"dsdb:schema update allowed"=true' allowed modify schema samba4
# or can add in smb.conf into section [global]
RUN     ldbadd -H /opt/samba4/private/sam.ldb /tmp/qmail.samba4-at.ldif \
            --option="dsdb:schema update allowed"=true
RUN     ldbadd -H /opt/samba4/private/sam.ldb /tmp/qmail.samba4-oc.ldif \
            --option="dsdb:schema update allowed"=true
# extanted user schema
RUN     ldbmodify -H /opt/samba4/private/sam.ldb /tmp/qmail.samba4-update-user-oc.ldif \
            --option="dsdb:schema update allowed"=true
# it does the "mail" optional
RUN     ldbmodify -H /opt/samba4/private/sam.ldb /tmp/qmail.samba4-qmailUser-downgrade.ldif \
            --option="dsdb:schema update allowed"=true

# tell the port number the container should expose
EXPOSE 22
