#!/usr/bin/env python3

'''
This's adding line option in section.
Examle: ./smb_add_param_to_section.py /etc/samba/smb.conf global 'guest ok = No'
'''

from sys import argv

configfile, section, option  = argv[1:]

conf_dict = {}
with open(configfile, 'r+') as f:
    lines = f.readlines()
    id_max = len(lines)
    flag = False
    count = -1
    for line in lines:
        count += 1
        if section in line:
            flag = True
            continue
        if flag and line != '\n' and line.strip()[0] == '[':
            lines.insert(count -1, ' '*8 + option + '\n')
            break
        elif flag and count == id_max - 1:
            lines.append(' '*8 + option + '\n')
            break

    f.seek(0)
    f.truncate()
    f.write(''.join(lines))
