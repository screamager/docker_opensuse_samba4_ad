#!/usr/bin/env bash


LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

sshpass -p $ROOTPASS ssh \
    -o "UserKnownHostsFile=/dev/null" \
    -o "StrictHostKeyChecking=no" \
    root@`sudo docker inspect -f "{{ .NetworkSettings.IPAddress }}" $CNAME`
