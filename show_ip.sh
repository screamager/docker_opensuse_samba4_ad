#!/usr/bin/env bash


LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

# it's show ip
sudo docker inspect -f "{{ .NetworkSettings.IPAddress }}" $CNAME
