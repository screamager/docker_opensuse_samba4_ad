#!/usr/bin/env bash


LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

# for not running docker, use save:
sudo docker save $INAME | gzip > $INAME.tar.gz

# for running or paused docker, use export:
#sudo docker export $INAME | gzip > $INAME.tar.gz

# load
#sudo gunzip -c $INAME.tar.gz | docker load
