#!/usr/bin/env bash


LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

# run and go to bash
#sudo docker run --rm --privileged -ti -e 'container=docker' -v /sys/fs/cgroup:/sys/fs/cgroup:ro $INAME /bin/bash

# run as daemon
############################
sudo docker run \
  -dti --rm \
  -p 139:139 -p 445:445 \
  -p 137:137/udp -p 138:138/udp \
  --name $CNAME \
  -h $FULLHOSTNAME \
  --privileged  -e 'container=docker' -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
  $INAME \
  /usr/lib/systemd/systemd --system

# edit /etc/resolv.conf
sshpass -p $ROOTPASS ssh \
    -o "UserKnownHostsFile=/dev/null" \
    -o "StrictHostKeyChecking=no" \
    root@`sudo docker inspect -f "{{ .NetworkSettings.IPAddress }}" $CNAME` \
    echo -e "search $DOMAIN\nnameserver $HOSTNAME" > /etc/resolv.conf

# it's show ip
############################
sudo docker inspect -f "{{ .NetworkSettings.IPAddress }}" $CNAME
