#!/usr/bin/env bash


LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

sudo docker build \
    --build-arg ROOTPASS=$ROOTPASS \
    --build-arg ADMINPASS=$ADMINPASS \
    --build-arg DOMAIN=$DOMAIN \
    --build-arg HOSTNAME=$HOSTNAME \
    --build-arg NETBIOSNAME=$NETBIOSNAME \
    -t $INAME .

