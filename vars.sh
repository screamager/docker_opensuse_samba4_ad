#!/usr/bin/env bash


INAME="os-smb-ad"       # image name
CNAME=$INAME                    # container name
DOMAIN="example.local"
NETBIOSNAME=`echo "${DOMAIN^^}"|sed 's/\..*//'`
HOSTNAME="file-server"
FULLHOSTNAME="${HOSTNAME}.$DOMAIN"

ROOTPASS='pamPapa^4'		# root password
ADMINPASS='Pa5w0rd!' # password for domain user "administrator"

SMBPACKVER='samba4<4.7'
